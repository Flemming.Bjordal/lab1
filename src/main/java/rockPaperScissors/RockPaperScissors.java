package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random; // Need this for the AI.

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
    
        boolean keepPlaying = true;

        while (keepPlaying) {
            System.out.println("Let's play round " + roundCounter);
            String userMove = getUserInput("Your choice (Rock/Paper/Scissors)?", true);

            // Get indexes of moves to make it easier to decide the winner later on.
            int userIndex = rpsChoices.indexOf(userMove);
            int compIndex = randChoice();

            // Get vital String-values for printing round results.
            String compMove = rpsChoices.get(compIndex);
            String resultString = getResult(userIndex, compIndex); // (Also updates the scores.)

            // Declare the winner!
            System.out.printf("Human chose %s, computer chose %s. %s\n",userMove,compMove,resultString);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            
            // Computer asks for a rematch.
            String userYN = getUserInput("Do you wish to continue playing? (y/n)?", false);
            if (userYN.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
            // Update roundCounter for the next round.
            roundCounter += 1;
        }

    }

    public int randChoice() { 
        //Returns a random integer from 0 to 2 (for choosing the computer's move).
        //Source: https://www.geeksforgeeks.org/generating-random-numbers-in-java/
        Random rand = new Random();
        int randIndex = rand.nextInt(3);
        return randIndex;
    }

    public String getUserInput(String prompt, boolean isChoosingMove) {
        
        // To be used when ensuring user inputs a valid response to a ["y"/"n"] question.
        List<String> userContinue = Arrays.asList("y", "n");

        // Repeat prompting the user until a valid response is given. 
        while (true) {
            String userPrompt = readInput(prompt);
            if (isChoosingMove) {
                if (rpsChoices.contains(userPrompt)) {
                    return userPrompt;
                }
            }
            else { // If isChoosingMove is false, the user must respond ["y"/"n"].
                if (userContinue.contains(userPrompt)) {
                    return userPrompt;
                }
            }
            System.out.println("I do not understand " + userPrompt + ". Could you try again?");
        }
    }

    public String getResult(int userIndex, int compIndex) {
        // String to be filled in later.
        String resultString = "";

        if (userIndex == compIndex) {
            // Round is tied.
            resultString += "It's a tie!"; 
        }

        else if (userIndex == (compIndex + 1) % 3) {
            // Every element in the rpsChoices list loses to the 'next' element of the list
            // Element 2 loses to element 0, hence the % 3.
            resultString += "Human wins!";
            humanScore += 1;
        }
        
        else {
            // If the round untied and human didn't win -> computer wins.
            resultString += "Computer wins!";
            computerScore += 1;
        }
        
        return resultString;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
